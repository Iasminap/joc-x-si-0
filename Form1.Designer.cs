﻿namespace x_si_0
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelx = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.punctajx = new System.Windows.Forms.Label();
            this.punctaj0 = new System.Windows.Forms.Label();
            this.btnjocnou = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.INCEPE = new System.Windows.Forms.Label();
            this.incepelbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.AllowDrop = true;
            this.button1.Font = new System.Drawing.Font("Ravie", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(181, 122);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 65);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Ravie", 10.8F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(273, 122);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 65);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Ravie", 10.8F, System.Drawing.FontStyle.Bold);
            this.button3.Location = new System.Drawing.Point(363, 122);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 65);
            this.button3.TabIndex = 2;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Ravie", 10.8F, System.Drawing.FontStyle.Bold);
            this.button4.Location = new System.Drawing.Point(181, 209);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 65);
            this.button4.TabIndex = 3;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Ravie", 10.8F, System.Drawing.FontStyle.Bold);
            this.button5.Location = new System.Drawing.Point(273, 209);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 65);
            this.button5.TabIndex = 4;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Ravie", 10.8F, System.Drawing.FontStyle.Bold);
            this.button6.Location = new System.Drawing.Point(363, 209);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 65);
            this.button6.TabIndex = 5;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Ravie", 10.8F, System.Drawing.FontStyle.Bold);
            this.button7.Location = new System.Drawing.Point(181, 296);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 65);
            this.button7.TabIndex = 6;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Ravie", 10.8F, System.Drawing.FontStyle.Bold);
            this.button8.Location = new System.Drawing.Point(273, 296);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 65);
            this.button8.TabIndex = 7;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Ravie", 10.8F, System.Drawing.FontStyle.Bold);
            this.button9.Location = new System.Drawing.Point(363, 296);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 65);
            this.button9.TabIndex = 8;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(333, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 32);
            this.label1.TabIndex = 9;
            this.label1.Text = "Scor jucator 2(0): ";
            // 
            // labelx
            // 
            this.labelx.AutoSize = true;
            this.labelx.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelx.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelx.Location = new System.Drawing.Point(81, 23);
            this.labelx.Name = "labelx";
            this.labelx.Size = new System.Drawing.Size(241, 32);
            this.labelx.TabIndex = 10;
            this.labelx.Text = "Scor jucator 1(X): ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 36F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(708, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 72);
            this.label2.TabIndex = 11;
            this.label2.Text = "X SI 0";
            // 
            // punctajx
            // 
            this.punctajx.AutoSize = true;
            this.punctajx.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.punctajx.ForeColor = System.Drawing.Color.Blue;
            this.punctajx.Location = new System.Drawing.Point(152, 55);
            this.punctajx.Name = "punctajx";
            this.punctajx.Size = new System.Drawing.Size(41, 44);
            this.punctajx.TabIndex = 12;
            this.punctajx.Text = "0";
            // 
            // punctaj0
            // 
            this.punctaj0.AutoSize = true;
            this.punctaj0.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.punctaj0.ForeColor = System.Drawing.Color.Blue;
            this.punctaj0.Location = new System.Drawing.Point(414, 55);
            this.punctaj0.Name = "punctaj0";
            this.punctaj0.Size = new System.Drawing.Size(41, 44);
            this.punctaj0.TabIndex = 13;
            this.punctaj0.Text = "0";
            // 
            // btnjocnou
            // 
            this.btnjocnou.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnjocnou.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnjocnou.Location = new System.Drawing.Point(677, 296);
            this.btnjocnou.Name = "btnjocnou";
            this.btnjocnou.Size = new System.Drawing.Size(224, 69);
            this.btnjocnou.TabIndex = 14;
            this.btnjocnou.Text = "Joc nou";
            this.btnjocnou.UseVisualStyleBackColor = true;
            this.btnjocnou.Click += new System.EventHandler(this.btnjocnou_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(84, 408);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 32);
            this.label3.TabIndex = 15;
            this.label3.Text = "INCEPE:";
            // 
            // INCEPE
            // 
            this.INCEPE.AutoSize = true;
            this.INCEPE.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.INCEPE.ForeColor = System.Drawing.Color.Maroon;
            this.INCEPE.Location = new System.Drawing.Point(223, 408);
            this.INCEPE.Name = "INCEPE";
            this.INCEPE.Size = new System.Drawing.Size(0, 32);
            this.INCEPE.TabIndex = 16;
            // 
            // incepelbl
            // 
            this.incepelbl.AutoSize = true;
            this.incepelbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.incepelbl.Location = new System.Drawing.Point(229, 408);
            this.incepelbl.Name = "incepelbl";
            this.incepelbl.Size = new System.Drawing.Size(35, 32);
            this.incepelbl.TabIndex = 17;
            this.incepelbl.Text = "X";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 511);
            this.Controls.Add(this.incepelbl);
            this.Controls.Add(this.INCEPE);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnjocnou);
            this.Controls.Add(this.punctaj0);
            this.Controls.Add(this.punctajx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelx);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label punctajx;
        private System.Windows.Forms.Label punctaj0;
        private System.Windows.Forms.Button btnjocnou;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label INCEPE;
        private System.Windows.Forms.Label incepelbl;
    }
}

