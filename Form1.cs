﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace x_si_0
{
    public partial class Form1 : Form
    {
        Button[,] butoane = new Button[3, 3];
        int nr = 0;
      
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Matricebutoane();
        }
        public void Matricebutoane()
        {
            butoane[0, 0] = button1; butoane[0, 1] = button2; butoane[0, 2] = button3;
            butoane[1, 0] = button4; butoane[1, 1] = button5; butoane[1, 2] = button6;
            butoane[2, 0] = button7; butoane[2, 1] = button8; butoane[2, 2] = button9;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            nr++;
            if(nr%2==0)
            {
                butoane[0,0].Text = "0";
                butoane[0,0].ForeColor = Color.Green;

            }
            else
            {
                butoane[0,0].Text = "X";
                butoane[0,0].ForeColor = Color.Red;

            }
            verifica(butoane);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            nr++;
            if (nr % 2 == 0)
            {
                butoane[0,1].Text = "0";
                butoane[0,1].ForeColor = Color.Green;

            }
            else
            {
                butoane[0,1].Text = "X";
                butoane[0, 1].ForeColor = Color.Red;

            }
            verifica(butoane);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            nr++;
            if (nr % 2 == 0)
            {
                butoane[0,2].Text = "0";
                butoane[0, 2].ForeColor = Color.Green;

            }
            else
            {
                butoane[0,2].Text = "X";
                butoane[0,2].ForeColor = Color.Red;

            }
            verifica(butoane);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            nr++;
            if (nr % 2 == 0)
            {
                butoane[1,0].Text = "0";
                butoane[1,0].ForeColor = Color.Green;

            }
            else
            {
                butoane[1,0].Text = "X";
                butoane[1,0].ForeColor = Color.Red;

            }
            verifica(butoane);
        }
        private void button5_Click(object sender, EventArgs e)
        {
            nr++;
            if (nr % 2 == 0)
            {
                butoane[1,1].Text = "0";
                butoane[1,1].ForeColor = Color.Green;

            }
            else
            {
                butoane[1,1].Text = "X";
                butoane[1,1].ForeColor = Color.Red;

            }
            verifica(butoane);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            nr++;
            if (nr % 2 == 0)
            {
                butoane[1,2].Text = "0";
                butoane[1,2].ForeColor = Color.Green;

            }
            else
            {
                butoane[1,2].Text = "X";
                butoane[1,2].ForeColor = Color.Red;

            }
            verifica(butoane);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            nr++;
            if (nr % 2 == 0)
            {
                butoane[2,0].Text = "0";
                butoane[2,0].ForeColor = Color.Green;

            }
            else
            {
                butoane[2,0].Text = "X";
                butoane[2,0].ForeColor = Color.Red;

            }
            verifica(butoane);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            nr++;
            if (nr % 2 == 0)
            {
                butoane[2,1].Text = "0";
                butoane[2,1].ForeColor = Color.Green;

            }
            else
            {
                butoane[2,1].Text = "X";
                butoane[2,1].ForeColor = Color.Red;

            }
            verifica(butoane);

        }

        private void button9_Click(object sender, EventArgs e)
        {
            nr++;
            if (nr % 2 == 0)
            {
                butoane[2,2].Text = "0";
                butoane[2, 2].ForeColor = Color.Green;
            }
            else
            {
                butoane[2,2].Text = "X";
                butoane[2, 2].ForeColor = Color.Red;
            }
            verifica(butoane);
        }
        public void goleste(Button[,] butoane)
        {
            for (int i= 0;i< 3;i++)
            {
                for(int j=0;j<3;j++)
                {
                    butoane[i, j].Text = null;
                }
            }
        }
        public void verifica(Button[,] butoane)
        {
            if(!verificalinie(butoane) && !verificacoloana(butoane) && !verificadiagonalaprincipala(butoane) && !verificadiagonalasecundara(butoane))
            {
                verificaegalitate(butoane);
            }
            

        }
        public bool verificalinie(Button[,] butoane)
        {
            int i, j, ok = 0; ;
            
            for (i = 0; i < 3; i++)
            {
                bool complet = true;
                for (j = 1; j < 3; j++)
                {
                    if (butoane[i, j].Text != butoane[i, 0].Text)
                    {
                        complet = false;
                    }
                }
                if (complet == true)
                {
                    if (butoane[i, 0].Text == "X")
                    {
                        MessageBox.Show("Runda a fost castigata de primul jucator. ");
                        goleste(butoane);
                        int scorx = int.Parse(punctajx.Text);
                        scorx++;
                        punctajx.Text = scorx.ToString();
                        nr--;
                        incepelbl.Text = "X";
                    }
                    else
                    if (butoane[i, 0].Text == "0")
                    {
                        MessageBox.Show("Runda a fost castigata de al doilea jucator. ");
                        goleste(butoane);

                        int scor0 = int.Parse(punctaj0.Text);
                        scor0++;
                        punctaj0.Text = scor0.ToString();
                        nr--;
                        incepelbl.Text = "0";

                    }
                     ok = 1; 
                }
            }
            if (ok == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool verificacoloana(Button[,]butoane)
        {
            int i, j, ok = 0;
            for (j = 0; j < 3; j++)
            {
                bool complet = true;
                for (i = 1; i < 3; i++)
                {
                    if (butoane[i, j].Text != butoane[0, j].Text)
                    {
                        complet = false;
                    }
                }
                if (complet == true)
                {
                    if (butoane[0, j].Text == "X")
                    {
                        MessageBox.Show("Runda a fost castigata de primul jucator. ");
                        goleste(butoane);

                        int scorx = int.Parse(punctajx.Text);
                        scorx++;
                        punctajx.Text = scorx.ToString();
                        nr--;
                        incepelbl.Text = "X";

                    }
                    else
                    if (butoane[0, j].Text == "0")
                    {
                        MessageBox.Show("Runda a fost castigata de al doilea jucator. ");
                        goleste(butoane);

                        int scor0 = int.Parse(punctaj0.Text);
                        scor0++;
                        punctaj0.Text = scor0.ToString();
                        nr--;
                        incepelbl.Text = "0";

                    }
                    ok=1;
                }

            }
            if (ok == 1)
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        public bool verificadiagonalaprincipala(Button[,]butoane)
        {
            int ok = 0;
            if (butoane[0, 0].Text == butoane[1, 1].Text && butoane[1, 1].Text == butoane[2, 2].Text)
            {
                if (butoane[0, 0].Text == "X")
                {
                    MessageBox.Show("Runda a fost castigata de primul jucator. ");
                    goleste(butoane);
                    int scorx = int.Parse(punctajx.Text);
                    scorx++;
                    punctajx.Text = scorx.ToString();
                    nr--;
                    incepelbl.Text = "X";


                }
                else
                if (butoane[0, 0].Text == "0")
                {
                    MessageBox.Show("Runda a fost castigata de al doilea jucator. ");
                    goleste(butoane);
                    int scor0 = int.Parse(punctaj0.Text);
                    scor0++;
                    punctaj0.Text = scor0.ToString();
                    nr--;
                    incepelbl.Text = "0";


                }
                ok = 1 ;
            }
            if(ok==1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool verificadiagonalasecundara(Button[,]butoane)
        {
            int ok = 0;
            if (butoane[0, 2].Text == butoane[1, 1].Text && butoane[1, 1].Text == butoane[2, 0].Text)
            {
                if (butoane[0, 2].Text == "X")
                {
                    MessageBox.Show("Runda a fost castigata de primul jucator. ");
                    goleste(butoane);
                    int scorx = int.Parse(punctajx.Text);
                    scorx++;
                    punctajx.Text = scorx.ToString();
                    nr--;
                    incepelbl.Text = "X";


                }
                else
                if (butoane[0, 2].Text == "0")
                {
                    MessageBox.Show("Runda a fost castigata de al doilea jucator. ");
                    goleste(butoane);
                    int scor0 = int.Parse(punctaj0.Text);
                    scor0++;
                    punctaj0.Text = scor0.ToString();
                    nr--;
                    incepelbl.Text = "0";


                }
                ok = 1; 
            }
            if(ok==1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void verificaegalitate(Button[,]butoane)
        {
           
            bool gata = true;

            if (butoane[0, 0].Text == null || butoane[0, 1].Text == null || butoane[0, 2].Text == null || butoane[1, 0].Text == null
                || butoane[1, 1].Text == null || butoane[1, 2].Text == null || butoane[2, 0].Text == null
                || butoane[2, 1].Text == null || butoane[2, 2].Text == null)
            {
                gata = false;
            }
            if (gata == true)
            {
                MessageBox.Show("Niciun castigator!");
                goleste(butoane);
                

            }
          
        }
        private void btnjocnou_Click(object sender, EventArgs e)
        {
            punctaj0.Text = "0";
            punctajx.Text = "0";
            goleste(butoane);
            incepelbl.Text = "X";
            if (nr % 2 != 0)
            {
                nr++;
            }
        }
    }
}
